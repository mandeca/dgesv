
all: dgesv

dgesv: dgesv.c
	icc -mkl -o dgesv dgesv.c -g

O3: dgesv.c
	icc -mkl -o dgesv -O3 dgesv.c -g

fast: dgesv.c
	icc -mkl -o dgesv -fast dgesv.c -g

omp: dgesv.c
	icc -mkl -o dgesv -fast dgesv.c -g -fopenmp

debug: dgesv.c
	icc -mkl -o dgesv -DDEBUG dgesv.c -g

gcc: dgesv.c
	gcc -o dgesv -DGCC dgesv.c

test: dgesv.c
	gcc -o dgesv -DGCC -DTEST -DDEBUG dgesv.c -g

run:
	echo "Small test"
	./dgesv 2048
	echo "Medium test"
	./dgesv 4096
	echo "Large test"
	./dgesv 8192

clean:
	rm dgesv
