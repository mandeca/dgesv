#!/bin/sh
#SBATCH -t 06:00:00 # execution time hh:mm:ss *OB*
#SBATCH -n 1 #tasks (for example, MPI processes)
#SBATCH -c 24 #cores/task (for example, shared-mem threads/process)
#SBATCH -p cola-corta

sizes=(2048 4096 8192)
GCC_FLAGS="-Wall -Wextra -DGCC -Ofast -march=native -fipa-pta"
ICC_FLAGS="-Wall -Wextra -DGCC -fast -march=native"

module load gcc/8.3.0

gcc $GCC_FLAGS -fprofile-generate -o gcc_pgo_gen dgesv.c

for rep in {1..5}; do
	srun ./gcc_pgo_gen 2048
done
echo "profiling ended"

gcc $GCC_FLAGS -fprofile-use -o gcc_pgo_use dgesv.c
for rep in {1..3}; do
	for s in "${sizes[@]}"; do
		echo -n "gcc_pgo - "
		srun ./gcc_pgo_use $s
	done
done


module load intel/2018.3.222

icc $ICC_FLAGS -prof-gen -o icc_pgo_gen dgesv.c

for rep in {1..5}; do
	srun ./icc_pgo_gen 2048
done
echo "profiling ended"

icc $ICC_FLAGS -prof-use -o icc_pgo_use dgesv.c
for rep in {1..3}; do
	for s in "${sizes[@]}"; do
		echo -n "icc_pgo - "
		srun ./icc_pgo_use $s
	done
done

