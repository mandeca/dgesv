# HPC Tools 2021-2022 - dgesv

Assignments related to the dgesv LAPACK function for solving real systems of linear equations, for the course on HPC Tools from UDC's HPC master.

Authors: Sergio Alonso Pascual and Manuel de Castro Caballero.
