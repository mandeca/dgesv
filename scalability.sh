#! /bin/bash
#SBATCH -t 6:00:00
#SBATCH -c 24 
#SBATCH -p cola-corta 
#SBATCH -o scalability.out

. ./env.sh
make clean 
make omp

sizes=(2048 4096 8192)
threads=(1 2 4 8 16 24)
for iter in {1..3}; do
	for s in "${sizes[@]}"; do
		echo "executing size $s"
		for t in "${threads[@]}"; do
			export OMP_NUM_THREADS=$t
			srun ./dgesv $s
		done
	done
done
