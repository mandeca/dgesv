#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#ifndef GCC
	#include "mkl_lapacke.h"
#endif // GCC

#define elem(mat, i, j) mat[i * _N + j]

double* generate_matrix(int size) {
	double* matrix = (double*)malloc(sizeof(double) * size * size);
	srand(1);

#ifndef TEST
	for (int i = 0; i < size * size; i++) {
		matrix[i] = rand() % 100;
	}
#else  // TEST
	double tmp[] = {-1, 0, -4, 2, 3, 1, 1, 1, 0};
	memcpy(matrix, tmp, 9 * sizeof(double));
#endif // !TEST

	return matrix;
}

void print_matrix(const char* name, double* matrix, int size) {
	printf("matrix: %s \n", name);

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			printf("%f ", matrix[i * size + j]);
		}
		printf("\n");
	}
}

int is_nearly_equal(double x, double y) {
	const double epsilon = 1e-5; // Some small number.
	return abs(x - y) <= epsilon * abs(x);
}

int check_result(double* bref, double* b, int size) {
	for (int i = 0; i < size * size; i++) {
		if (!is_nearly_equal(bref[i], b[i])) return 0;
	}

	return 1;
}

void copy_matrix(int n, int m, double* src, double* dst) {
#define _N m
	// #pragma omp for
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			elem(dst, i, j) = elem(src, i, j);
#undef _N
}

static inline void my_memcpy(double* restrict dst, double* restrict src, size_t size) {
	// #pragma omp parallel for
	for (size_t i = 0; i < size; i++) {
		dst[i] = src[i];
	}
}

/**
 * [in]
 * n -> number of equations / order of matrix
 * nrhs -> number of right hand sides
 * *a -> n-by-n coefficient matrix
 * lda -> leading dimension of *a
 * *b -> n-by-nrhs matrix, right hand side matrix
 * ldb -> leading dimension of *b
 *
 * [out]
 * a -> factors L and U (unit diagonal of L omitted)
 * ipiv -> array of size n; pivot indices
 * b -> if return == 0, n-by-nhrs solution matrix
 * return -> if == 0, successful exit; if == -i, i-th argument had an illegal value; if == i, U[i, i] == 0, so the
 * solution cannot be computed
 */
int my_dgesv(int n, int nrhs, double* restrict a, int lda, int* restrict ipiv, double* restrict b, int ldb) {
#define _N lda

	if (n < 0) return -1;
	if (nrhs < 0) return -2;
	if (a == NULL) return -3;
	if (!(lda >= 1 && lda >= n)) return -4;
	if (ipiv == NULL) return -5;
	if (b == NULL) return -6;
	if (!(ldb >= 1 && ldb >= n)) return -7;

	/* Init ipiv */
	for (int i = 0; i < n; i++)
		ipiv[i] = i;

	/* Permute matrix */
	double tmp[lda], tmp2[lda];
	int	   computable = 1;
	for (int i = 0; i < n; i++) {
		if (is_nearly_equal(elem(a, i, i), 0)) {
			computable = 0;
			/* If a[i][i] is 0, we have to permute row i. */
			for (int j = 0; j < n; j++) {
				/* Permute with a row different to i whose i-th element is not 0 */
				/* Also, compute with a row so that a new diagonal 0 is not created */
				if (i != j && elem(a, j, i) != 0 && elem(a, i, j) != 0) {
					// a
					my_memcpy(tmp, &elem(a, i, 0), lda);
					my_memcpy(&elem(a, i, 0), &elem(a, j, 0), lda);
					my_memcpy(&elem(a, j, 0), tmp, lda);
					// b
					my_memcpy(tmp2, &elem(b, i, 0), nrhs);
					my_memcpy(&elem(b, i, 0), &elem(b, j, 0), nrhs);
					my_memcpy(&elem(b, j, 0), tmp2, nrhs);

					/* Write permutation. Used later to permute b */
					ipiv[i] = j;
					ipiv[j] = i;

					computable = 1;
					break;
				}
			}

			if (computable == 0) {
				/* Checked all rows and couldn't find a viable permutation */
				return i; // TODO: temporal. To be exactly like LAPACKE_dgesv, L and U factors should be computed even
						  // if the system is unsolvable
			}
		}
	}

	/* L: initialize to identity matrix */
	double* L = (double*)calloc(n * n, sizeof(double));
	double* U = (double*)calloc(n * n, sizeof(double));
	for (int i = 0; i < n; i++) {
		elem(L, i, i) = 1;
	}

	/* U: initialize to a */
	copy_matrix(n, n, a, U);
#pragma omp parallel
	{
		for (int i = 0; i < n; i++) {
			const double U_ii	  = elem(U, i, i);
			const double inv_U_ii = 1 / U_ii;

#pragma omp for nowait
			for (int i2 = i + 1; i2 < n; i2++) {
				elem(L, i2, i) = elem(U, i2, i) * inv_U_ii;
			}

			/* Make zeroes */
#pragma omp for
			for (int i2 = i + 1; i2 < n; i2++) {
				for (int j = i; j < lda; j++) {
					elem(U, i2, j) -= elem(U, i, j) * elem(L, i2, i);
				}
			}
		}

#ifdef DEBUG
		print_matrix("L", L, n);
		printf("\n\n");
		print_matrix("U", U, n);
		printf("\n\n");
#endif // DEBUG

		/* Diagonalize and solve matrices */
		// L
		for (int i = 0; i < n; i++) {
/* Row reduction */
#pragma omp for
			for (int i2 = i + 1; i2 < n; i2++) {
				double L_ij = elem(L, i2, i);

				for (int j = 0; j < nrhs; j++) {
					elem(b, i2, j) -= elem(b, i, j) * L_ij;
				}
			}
		}
#ifdef DEBUG
		print_matrix("Diagonal L", L, n);
		printf("\n");
#endif // DEBUG

		// U
		for (int i = n - 1; i >= 0; i--) {
			double U_ii = elem(U, i, i);

#pragma omp for
			for (int j = 0; j < nrhs; j++) {
				elem(b, i, j) /= U_ii;
			}

/* Row reduction */
#pragma omp for
			for (int i2 = i - 1; i2 >= 0; i2--) {
				double U_ij = elem(U, i2, i);

				for (int j = 0; j < nrhs; j++) {
					elem(b, i2, j) -= elem(b, i, j) * U_ij;
				}
			}
		}
	}
#ifdef DEBUG
	print_matrix("Diagonal U", U, n);
	printf("\n\n");
#endif // DEBUG

	// /* Pack L and U in a */
	// for (int i = 0; i < n; i++) {
	// 	for (int j = 0; j < n; j++) {
	// 		if (j >= i) {
	// 			elem(a, i, j) = elem(U, i, j);
	// 		} else {
	// 			elem(a, i, j) = elem(L, i, j);
	// 		}
	// 	}
	// }
	free(L);
	free(U);
	return 0;
#undef _N
}

#ifdef GCC
typedef int MKL_INT;
#endif // GCC

int main(__attribute__((__unused__)) int argc, char* argv[]) {
#ifndef TEST
	int size = atoi(argv[1]);
#else  // TEST
	int size = 3;
#endif // TEST

	double *a, *b;
	a = generate_matrix(size);
#ifndef GCC
	double *aref, *bref;
	aref = generate_matrix(size);
	bref = generate_matrix(size);
#endif // GCC
#ifndef TEST
	b = generate_matrix(size);
#else  // TEST
	b		 = malloc(9 * sizeof(double));
	b[0]	 = 1;
	b[3]	 = 1;
	b[6]	 = 1;
#endif // !TEST

	// print_matrix("A", a, size);
	// print_matrix("B", b, size);

	// Using MKL to solve the system
	MKL_INT n = size, nrhs = size, lda = size, ldb = size;

	clock_t tStart;
#ifndef GCC
	MKL_INT* ipiv = (MKL_INT*)malloc(sizeof(MKL_INT) * size);
	tStart		  = omp_get_wtime();
	LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, nrhs, aref, lda, ipiv, bref, ldb);
	printf("Time taken by MKL: %.2fs\n", omp_get_wtime() - tStart);
	free(ipiv);
#endif // !GCC

	// Using own implementation
	tStart		   = omp_get_wtime();
	MKL_INT* ipiv2 = (MKL_INT*)malloc(sizeof(MKL_INT) * size);
	my_dgesv(n, nrhs, a, lda, ipiv2, b, ldb);
	printf("Time taken by my implementation: %.2fs\n", omp_get_wtime() - tStart);
	free(ipiv2);
#ifndef GCC
	if (check_result(bref, b, size) == 1)
		printf("Result is ok!\n");
	else
		printf("Result is wrong!\n");
	free(aref);
	free(bref);
#endif // !GCC
	free(b);
	free(a);
#ifdef DEBUG
	print_matrix("X", b, size);
	print_matrix("Xref", bref, size);
#endif // DEBUG

	return 0;
}
